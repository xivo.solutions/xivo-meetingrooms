# XiVO Meeting Rooms

This is the XiVO Meeting Rooms installer project.

Currently it only consists of a compose file.


## Update Jitsi components

**TODO** We should rework our yml. Idea is to have
- in xivo-meetingroom.yml: the raw upstream jitsi yml file
- in an override file (xivo-meetingroom.override.yml): our modifications

When we want to update Jitsi components (let's say we are using version stable-5870 and you want to go to version `stable-5963`) you have to:
- clone repo https://github.com/jitsi/docker-jitsi-meet
- checkout the tag of the version you want to upgrade to e.g. `stable-5963`
- from the repo docker-jitsi-meet, copy the yml file `docker-compose.yml` in our repository xivo-meetingroom
- then compare the upstream and our yml to update our yml file accordingly
- then do the same by comparing the upstream `jigasi.yml` and the jigasi section in our yml file
- then add, if needed, new vars to our `.env` that we document in the doc
- then change the base image of our components (see, for example, xivo.solutions/xivo-web-jitsi/Dockerfile>)