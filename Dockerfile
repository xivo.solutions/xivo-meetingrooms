FROM alpine

ARG TARGET_VERSION
LABEL version=${TARGET_VERSION}

RUN mkdir -p /etc/xivo-meetingrooms
ADD xivo-meetingrooms.yml /etc/xivo-meetingrooms/